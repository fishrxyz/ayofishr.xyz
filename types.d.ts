type Article = {
  id: string;
  title: string;
  date: string;
};
