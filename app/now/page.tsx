import React from "react";
import PageLayout from "../components/layouts/PageLayout";
import nowData from "../data/now.json";
import NowSubSection from "../components/now/NowSubSection";

export const metadata = {
  title: "Now",
  description: "this is the now page where I share what I am currently up to",
};

// import data
const NowPage = () => {
  // import data and then display the data
  const { sections } = nowData;
  return (
    <PageLayout
      title="/now"
      description="this is the now page where I share what I am currently up to"
    >
      {sections.map((section, idx) => (
        <NowSubSection info={section} key={idx} />
      ))}
    </PageLayout>
  );
};

export default NowPage;
