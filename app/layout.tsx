import type { Metadata } from "next";
import { Inter } from "next/font/google";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: {
    template: "%s | Karim Cheurfi - Software Developer, Cloud Engineer, Maker.",
    default: "Karim Cheurfi - Software Developer, Cloud Engineer, Maker.",
  },
  description:
    "I'm a full-stack software developer and recently certified cloud engineer currently based in Paris, FR.",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>{children}</body>
    </html>
  );
}
