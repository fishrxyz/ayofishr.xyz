import fs from "fs";
import path from "path";
import matter from "gray-matter";
import { remark } from "remark";
import html from "remark-html";

const postsDirectory = path.join(process.cwd(), "posts");

export const getSortedPostsData = () => {
  const fileNames = fs.readdirSync(postsDirectory);
  const allPostsData = fileNames.map((filename) => {
    // extract post id
    const postId = filename.replace(/\.md$/, "");

    // read markdown file as string
    const fullPathToFile = path.join(postsDirectory, filename);
    const fileContents = fs.readFileSync(fullPathToFile, "utf8");

    // parse the file content as markdown
    const parsedFile = matter(fileContents);

    const article: Article = {
      id: postId,
      title: parsedFile.data.title,
      date: parsedFile.data.date,
    };

    return article;
  });

  return allPostsData.sort((a, b) => (a.date < b.date ? 1 : -1));
};

export const getPostData = async (id: string) => {
  const pathToFile = path.join(postsDirectory, `${id}.md`);
  const fileContents = fs.readFileSync(pathToFile, "utf8");

  const parsedFile = matter(fileContents);

  const processedContents = await remark()
    .use(html)
    .process(parsedFile.content);

  const htmlContent = processedContents.toString();

  const postWithHTML: Article & { htmlContent: string } = {
    id,
    title: parsedFile.data.title,
    date: parsedFile.data.date,
    htmlContent,
  };

  return postWithHTML;
};
