import Blurb from "./components/home/Blurb";
import Header from "./components/home/Header";
import Nav from "./components/home/Nav";

export default function Home() {
  return (
    <main>
      <Header />
      <Blurb />
      <Nav />
    </main>
  );
}
