import React from "react";
import NotesItem from "./NotesItem.tsx";
import { getSortedPostsData } from "../../lib/posts.tsx";

const NotesList = () => {
  const posts = getSortedPostsData();

  return (
    <ul>
      {posts.map((post, idx) => (
        <NotesItem post={post} key={idx} />
      ))}
    </ul>
  );
};

export default NotesList;
