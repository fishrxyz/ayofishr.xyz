import React from "react";
import Link from "next/link";
import formatDate from "../../lib/date.ts";

type Props = {
  post: Article;
};

const NotesItem = ({ post }: Props) => {
  const { id, title, date } = post;
  const formattedDate = formatDate(date);
  return (
    <li>
      <Link href={`/notes/${id}`}>{title}</Link>
      <p>{formattedDate}</p>
    </li>
  );
};

export default NotesItem;
