import React from "react";

const NowSubSection = ({ info }: any) => {
  return (
    <div>
      <h3>{info.title}</h3>
      <ul>
        {info.tasks.map((task: any, idx: any) => (
          <li key={idx}>{task.text}</li>
        ))}
      </ul>
    </div>
  );
};

export default NowSubSection;
