import React from "react";

const PageLayout = ({ title, description, children }) => {
  return (
    <main className="fishr-page">
      <div className="fishr-page__container">
        hello
        <header>
          <div className="page-header__upper">
            <h2>LOGO HERE</h2>
            <span>{title}</span>
          </div>
          <div className="page-header__lower">
            <p>{description}</p>
          </div>
        </header>
        {children}
      </div>
    </main>
  );
};

export default PageLayout;
