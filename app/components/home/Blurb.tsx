import React from "react";

const Blurb = () => {
  return (
    <article>
      <p>
        Well hello and welcome to my internet garden ! My name is Karim Cheurfi
        (fishr pronounced backwards). I'm a software developer, cloud engineer
        and live streamer currently based in Paris, France.
      </p>

      <p>
        I'm the maker of ngnr.club, a "link-in-bio" service for engineers, along
        with many other things.
      </p>
    </article>
  );
};

export default Blurb;
