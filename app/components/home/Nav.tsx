import Link from "next/link";
import React from "react";

const Nav = () => {
  return (
    <nav>
      <ul>
        <li>
          <Link href="/about">/About</Link>
        </li>
        <li>
          <Link href="/notes">/Blog</Link>
        </li>
        <li>
          <Link href="/now">/Now</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
