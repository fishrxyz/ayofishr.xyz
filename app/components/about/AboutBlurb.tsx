import { EMAIL, TWITCH_URL, GITLAB_URL } from "../../lib/constants";
import AboutTitle from "./AboutTitle.tsx";

const AboutBlurb = () => {
  return (
    <section>
      <AboutTitle text="whoami" />
      <p>
        I'm a linux nerd turned full stack developer with 7 years of experience.
        I've recently decided to transition to DevOps/SRE/Cloud Engineering and
        spent much of 2022 studying to get myself certified. (see below)
      </p>
      <p>
        Over the years, I've worked in both England and France, for development
        studios, startups, consulting firms and myself.
      </p>
      <p>
        Other than that, I host live coding stream on my{" "}
        <a href={TWITCH_URL}>twitch channel</a>, where I build most of the fun
        projects mentioned below (and on <a href={GITLAB_URL}>GitLab</a>)
      </p>
      <p>
        I'm currently looking for an opportunity in the cloud/platform
        engineering space. Preferably in the Netherlands so if you know of
        anything, feel free to <a href={EMAIL}>get in touch !</a>
      </p>
    </section>
  );
};

export default AboutBlurb;
