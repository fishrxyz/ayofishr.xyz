import AboutTitle from "./AboutTitle";
import projectsInfo from "../../data/projects.json";

const AboutProjects = () => {
  const { projects } = projectsInfo;
  return (
    <section>
      <AboutTitle text="ls /home/fishr/projects" />
      <p>
        The following is a list of projects I've launched or am currently
        maintaining.
      </p>
      <div>
        {projects.map((project, idx) => (
          <div key={idx}>
            <h4>
              <a href={project.url}>{project.name}</a>
            </h4>
            <p>{project.about}</p>
          </div>
        ))}
      </div>
    </section>
  );
};

export default AboutProjects;
