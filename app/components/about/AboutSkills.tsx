import AboutTitle from "./AboutTitle.tsx";

const AboutSkills = () => {
  return (
    <section>
      <AboutTitle text="cat ~/skills.txt" />
      <p>
        Below is a non exhaustive list of the languages/tools I use in my work:{" "}
      </p>
      <ul>
        <li>Golang</li>
        <li>Python</li>
        <li>Node.js</li>
        <li>SQL</li>
      </ul>
      <ul>
        <li>Terraform</li>
        <li>Cloudformation</li>
        <li>Ansible</li>
        <li>Vagrant</li>
      </ul>
      <ul>
        <li>Kubernetes</li>
        <li>Gitlab CI</li>
        <li>P(react).js</li>
        <li>AWS/GCP</li>
      </ul>
    </section>
  );
};

export default AboutSkills;
