import AboutTitle from "./AboutTitle.tsx";

const AboutGear = () => {
  return (
    <section>
      <AboutTitle text="cat ~/gear.txt" />
      <p>
        I'm basically a unixporn normie which means I do all of my work on a
        used thinkpad X250 running arch linux with a custom dwm rice, dmenu and
        anime wallpapers.{" "}
      </p>
      <p>
        UPDATE 2022: this is no longer the case. I now use a MacBook Pro with
        vagrant. I know, principles.
      </p>
      <p>
        UPDATE 2023: I have now moved my vagrant env to an EC2 instance. I
        called it{" "}
        <a href="https://gitlab.com/fishrxyz/wintermute">wintermute</a>
      </p>
    </section>
  );
};

export default AboutGear;
