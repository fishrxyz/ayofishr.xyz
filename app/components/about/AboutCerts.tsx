import certsInfo from "../../data/certs.json";
import AboutTitle from "./AboutTitle";

const AboutCerts = () => {
  const { certs } = certsInfo;
  return (
    <section>
      <AboutTitle text="cat /home/fishr/certs" />
      <p>These are the tech/cloud certifications I have completed.</p>
      <ul>
        {certs.map((cert, idx) => (
          <li key={idx}>
            {cert.name} - ({cert.date}) - <a href={cert.badge}>Badge</a>
          </li>
        ))}
      </ul>
    </section>
  );
};

export default AboutCerts;
