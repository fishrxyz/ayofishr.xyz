import React from "react";
import PageLayout from "../components/layouts/PageLayout";
import AboutBlurb from "../components/about/AboutBlurb.tsx";
import AboutCerts from "../components/about/AboutCerts.tsx";
import AboutProjects from "../components/about/AboutProjects.tsx";
import AboutSkills from "../components/about/AboutSkills.tsx";
import AboutGear from "../components/about/AboutGear.tsx";

export const metadata = {
  title: "About Me",
  description:
    "Find out about the projects I'm currently working on, my certifications and my skills.",
};

const About = () => {
  return (
    <PageLayout title="/about" description={""}>
      <h2>About</h2>
      <AboutBlurb />
      <AboutCerts />
      <AboutProjects />
      <AboutSkills />
      <AboutGear />
    </PageLayout>
  );
};

export default About;
