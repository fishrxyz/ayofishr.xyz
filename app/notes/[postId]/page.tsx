import React from "react";
import Link from "next/link";
import { notFound } from "next/navigation";
import { getPostData, getSortedPostsData } from "../../lib/posts.tsx";
import formatDate from "../../lib/date";

export const generateMetadata = ({
  params,
}: {
  params: { postId: string };
}) => {
  const posts = getSortedPostsData();
  const { postId } = params;

  const post = posts.find((post) => post.id === postId);

  if (!post) {
    return {
      title: "Post Not Found",
    };
  }

  return {
    title: post.title,
  };
};

const ArticlePage = async ({ params }: { params: { postId: string } }) => {
  const posts = getSortedPostsData();
  const { postId } = params;

  if (!posts.find((post) => post.id === postId)) {
    return notFound();
  }

  const { title, date, htmlContent } = await getPostData(postId);
  const pubDate = formatDate(date);

  return (
    <main>
      <h1>{title}</h1>
      <p>{pubDate}</p>
      <article>
        <section dangerouslySetInnerHTML={{ __html: htmlContent }} />
        <Link href="/notes">back to posts</Link>
      </article>
    </main>
  );
};

export default ArticlePage;
