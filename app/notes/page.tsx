import React from "react";
import PageLayout from "../components/layouts/PageLayout";
import NotesList from "../components/notes/NotesList.tsx";

export const metadata = {
  title: "Articles",
  description: "My thoughts, musings, rambblings and technical notes",
};

const Notes = () => {
  return (
    <PageLayout
      title="/notes"
      description="This page is a collection of my thoughts, musings, ramblings and technical notes"
    >
      <h2>ALL BLOG POSTS</h2>
      <NotesList />
    </PageLayout>
  );
};

export default Notes;
